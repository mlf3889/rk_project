﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RK
{




    public partial class Form1 : Form
    {
        int width = 1000;
        int height = 600;
        double r = 0;
        int x = 0;
        int y = 0;
        double theta = 0;
        double thickness = 0;
        double minAngle = 0;
        double maxAngle = 0;

        int[] center = new int[2];
        int[] R = Enumerable.Range(0, 2).ToArray();
        int grav = 20000;
        double[] X = new double[3];
        double delta = 0.5;
        int C = 1900;
        int[] K = new int[4];
        int[] KK = new int[4];
        int[] KKK = new int[4];


        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 4; i++) {
                K[i] = 0;
                KK[i] = 0;
                KKK[i] = 0;
            }
            R[0] = 100;
            R[1] = 0;
            X[0] = 0.00;
            X[1] = 0.0050;
            X[2] = 0.00;
            center[0] = width / 2;
            center[1] = height / 2;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void polarCircle()
        {
            x = Convert.ToInt16(center[0] + r * Math.Cos(theta));
            y = Convert.ToInt16(center[1] - r * Math.Sin(theta));
            //ctx.fillStyle = "#FF0000";
            //ctx.strokeStyle = "#FF0000";
            //ctx.beginPath();
            //ctx.arc(x, y, 4, 0, 6.28);
            //ctx.fill();
            // return;


            label1.Text = Convert.ToString(x); // list values for debugging
            label2.Text = Convert.ToString(y); // debugging
            pictureBox1.Location = new Point(x, y);

        }


        private void ellpiticalPath() {

            K[0] = Convert.ToInt16( C * X[1] * X[1]);
            KK[0] = Convert.ToInt16(X[2]);
            KKK[0] = Convert.ToInt16(- X[1] + grav / C / C);

            K[1] = Convert.ToInt16(C * (X[1] + KK[0] * delta / 2) * (X[1] + KK[0] * delta / 2));
            KK[1] = Convert.ToInt16(X[2] + KKK[0] * K[1] * delta / 2);
            KKK[1] = Convert.ToInt16(-X[1] - KK[0] * K[1] * delta / 2 + grav / C / C);

            K[2] = Convert.ToInt16(C * (X[1] + KK[1] * delta / 2) * (X[1] + KK[0] * delta / 2));
            KK[2] = Convert.ToInt16(X[2] + KKK[1] * K[2] * delta / 2);
            KKK[2] = Convert.ToInt16(-X[1] - KK[1] * K[2] * delta / 2 + grav / C / C);

            K[3] = Convert.ToInt16(C * (X[1] + KK[2] * delta) * (X[1] + KK[2] * delta));
            KK[3] = Convert.ToInt16(X[2] + KKK[2] * K[3] * delta);
            KKK[3] = Convert.ToInt16(-X[1] - KK[3] * K[3] * delta + grav / C / C);

            X[0] = X[0] + (K[0] + 2 * K[1] + 2 * K[2] + K[3]) * delta / 6;
            X[1] = X[1] + (KK[0] + 2 * KK[1] + 2 * KK[2] + KK[3]) / 6 * (K[0] + 2 * K[1] + 2 * K[2] + K[3]) * delta / 6;
            X[2] = X[2] + (KKK[0] + 2 * KKK[1] + 2 * KKK[2] + KKK[3]) * delta / 6 * (K[0] + 2 * K[1] + 2 * K[2] + K[3]) / 6;

            
            r = 1 / X[1];
            theta = X[0];
            thickness = 2;
            minAngle = 0;
            maxAngle = 6.28;

            polarCircle();
            System.Threading.Thread.Sleep(150); // sleep thread to slow process

        }

        private void button1_Click(object sender, EventArgs e)
        {
            while (true) {
                ellpiticalPath();
            }
        }
    }
}
